<?php
include("header_ae.php");
include("dbconnect.php");
include("sidemenu_ae.php");
/*
session_start();
$_SESSION['message']=' ';
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	//verify the pass are equal to each other
	if ($_POST['password'] == $_POST['confirmpassword'])
	{
	$adm_ID = $mysqli->real_escape_string($_POST['adm_ID']);
	$nume = $mysqli->real_escape_string($_POST['nume']);
	$prenume = $mysqli->real_escape_string($_POST['prenume']);
	$sediu = $mysqli->real_escape_string($_POST['sediu']);
	$email = $mysqli->real_escape_string($_POST['email']);
	$username = $mysqli->real_escape_string($_POST['username']);
	$password = md5($_POST['password']); //hash pass security

	$_SESSION['email']=$email;
	$_SESSION['username']=$username;
	
	$sql = " INSERT INTO administrator (adm_ID, nume, prenume, sediu, email, username, password)"
			.  VALUES ('$adm_ID','$nume','$prenume','$sediu','$email','$username','$password');
	}
	
	//if the query is successful redirect to homepage
	if($mysqli)->query($sql) === true {
		$_SESSION['message'] = "Inregistrare incheiata cu succes!";
		header("location: index_ae.php";
	}
	else {
		$_SESSION['message'] = "Inregistrare nefinalizata!";
	}
	else {
		$_SESSION['message'] = "Parolele nu corespund";
	}
	
}	*/
?>
<html>
<head>

<title> AdminExpress </title>
<link href="https://fonts.googleapis.com/css?family=Poor+Story|Titillium+Web" rel="stylesheet"> 
<link rel="stylesheet" href="style/form.css" type="text/css">
</head>
<body> 

<div class="body-content">
  <div class="module">
    <h1>Creaza cont nou</h1>
    <form class="form" action="form.php" method="post" enctype="multipart/form-data" autocomplete="off">
      <div class="alert alert-error"> <?= $_SESSION['message']?></div>
      <input type="text" placeholder="Numar inregistrare" name="adm_ID" required />
	  <input type="text" placeholder="Nume" name="nume" required />
	  <input type="text" placeholder="Prenume" name="prenume" required />
      <input type="text" placeholder="Sediu" name="sediu" required />
	  <input type="text" placeholder="Numar telefon" name="telefon" required />
	  <input type="email" placeholder="Email" name="email" required />
	  <input type="text" placeholder="Nume Utilizator" name="username" required />
      <input type="password" placeholder="Parola" name="password" autocomplete="new-password" required />
	  <input type="password" placeholder="Confirma Parola" name="confirmpassword" autocomplete="new-password" required />
      <input type="submit" value="Inregistrare" name="register" class="btn btn-block btn-primary" />
    </form>
  </div>
</div>

</body>
<?php
	include("footer_ae.php");
	?>
</html>