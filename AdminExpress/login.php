<?php
include("header_ae.php");
include("dbconnect.php");
include("sidemenu_ae.php");
?>
<html>
<head>

<title> AdminExpress </title>
<link href="https://fonts.googleapis.com/css?family=Poor+Story|Titillium+Web" rel="stylesheet"> 
<link rel="stylesheet" href="style/form.css" type="text/css">
</head>
<body> 
<div class="body-content">
  <div class="module">
    <h1>Autentificare</h1>
	<form class="form" action="form.php" method="post" enctype="multipart/form-data" autocomplete="off">
      <div class="alert alert-error"> <?= $_SESSION['message']?></div>
	  <input type="text" placeholder="Nume Utilizator" name="username" required />
      <input type="password" placeholder="Parola" name="password"  required />
	  <input type="submit" value="Autentificare" name="autentificare" class="btn btn-block btn-primary" />
    </form>
  </div>
</div>
</body>
<?php
	include("footer_ae.php");
	?>
</html>